import { Box, Typography, Avatar, TextField, Button } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { useState } from 'react';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [nameInput, setNameInput] = useState(name);
  const [emailInput, setEmailInput] = useState(email);

  const handleCancel = () => {
    setNameInput(name);
    setEmailInput(email);
  };

  return (
    <Card sx={{ ...sx }}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <TextField
            id="outlined-basic"
            label="Outlined"
            variant="outlined"
            value={nameInput.length > 0 ? nameInput : name}
            onChange={(e) => setNameInput(e.target.value)}
          />
          <TextField
            id="outlined-basic"
            label="Outlined"
            variant="outlined"
            value={emailInput.length > 0 ? emailInput : email}
            onChange={(e) => setEmailInput(e.target.value)}
          />
        </Box>
        <Box sx={{ display: 'flex' }}>
          <Button sx={{ color: 'red' }}>Commit</Button>
          <Button onClick={handleCancel}>Cancel</Button>
        </Box>
      </Box>
    </Card>
  );
};
